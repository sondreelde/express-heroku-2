
const cwd = process.cwd()
const path = require("path")
const express = require("express")
var morgan = require("morgan")
require("dotenv").config()

const app = express()

app.use(express.static(path.join(cwd,"public/build")))

const {
    PORT: port = 3000
} = process.env

app.listen(port, () => console.log(`Listening on port ${port}`))